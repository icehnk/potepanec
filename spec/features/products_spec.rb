require 'rails_helper'

RSpec.feature "Potepan Products", type: :feature do
  given(:product) { create(:product) }

  feature "Access show page" do
    background do
      visit potepan_product_path(product.id)
    end

    scenario "get product's information" do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end

    scenario "get correct title" do
      expect(page).to have_title "#{product.name} - BIGBAG Store"
    end

    scenario "get correct link to Home" do
      within '.navbar-right' do
        click_link "Home"
        expect(page).to have_current_path potepan_index_path
      end
      visit potepan_product_path(product.id)
      within '.breadcrumb' do
        click_link "Home"
        expect(page).to have_current_path potepan_index_path
      end
    end
  end
end
